#!/bin/bash
if [[ $1 == 'all' ]]
then
    echo "WARNING: all data in this directory and subdirectories will be erased!"
    read
    rm -rf ./*/*
    echo '{"queue":[]}' > ./queue/queue.json
else
    rm -rf ./queue/*
    rm -rf ./processing/*
    echo '{"queue":[]}' > ./queue/queue.json
fi
